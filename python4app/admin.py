from django.contrib import admin
from .models import usuario

# Register your models here.
class AdminRegistrado(admin.ModelAdmin):
    list_display = ["nombre","fecha"]
    class Meta:
        model = usuario

# Register your models here.
admin.site.register(usuario, AdminRegistrado)
