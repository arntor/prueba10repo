from django.apps import AppConfig


class Python4AppConfig(AppConfig):
    name = 'python4app'
