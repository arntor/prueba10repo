from django.db import models

# Create your models here.
class usuario(models.Model):
    nombre = models.CharField(max_length=50, null=True, blank=True)
    fecha = models.DateTimeField()

    def __str__(self):
        return self.nombre
